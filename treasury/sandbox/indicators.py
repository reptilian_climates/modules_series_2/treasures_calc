

def add_paths_to_system (paths):
	import pathlib
	from os.path import dirname, join, normpath
	import sys
	
	this_folder = pathlib.Path (__file__).parent.resolve ()	
	for path in paths:
		sys.path.insert (0, normpath (join (this_folder, path)))

add_paths_to_system ([
	'../structures',
	'../structures_pip'
])


import pandas as pd
import pandas_ta as ta
'''
	open
	high
	low
	close
	volume
'''


'''
	https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html
'''
d = {'col1': [1, 2], 'col2': [3, 4]}

df = pd.DataFrame ([{
	'datetime': '2030-05-05',
	'open': 10,
	'high': 11,
	'low': 9,
	'close': 10,
	'volume': 1000
}])

# VWAP requires the DataFrame index to be a DatetimeIndex.
# Replace "datetime" with the appropriate column from your DataFrame
df.set_index(pd.DatetimeIndex(df["datetime"]), inplace=True)

# Calculate Returns and append to the df DataFrame
df.ta.log_return(cumulative=True, append=True)
df.ta.percent_return(cumulative=True, append=True)

# New Columns with results
df.columns

# Take a peek
df.tail()




'''
	https://rethinkdb.com/api/python/table_list/
	
	REPL:
		r.db ('rethinkdb').table_list ().run ()
	
	PYTHON3:
		r.db ('rethinkdb').table_list ().run (conn)
		
	DATA EXPLORER:
		r.db ('rethinkdb').tableList ()
'''

